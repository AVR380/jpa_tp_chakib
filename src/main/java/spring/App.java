package spring;





import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import spring.config.AppConfig;
import spring.config.JPAConfig;
import spring.models.Adresse;
import spring.models.Employee;
import spring.models.Notes;
import spring.services.AdresseService;
import spring.services.EmployeeService;
import spring.services.NotesService;



/**
 * Hello world!
 *
 */


public class App 
{
    public static void main( String[] args )
    {
 

    	
    	AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    	

    	context.register(AppConfig.class, JPAConfig.class);
    	context.refresh();
    	

    	EmployeeService empService = context.getBean(EmployeeService.class);
    	
    	
    //			CRUD sur employee:
    //			(marche aussi sur notes et adresse)

    	
    	
    	//	Create
    	
    	Employee emp_temp = new Employee();
    	Adresse add_temp = new Adresse();
    	Notes notes_temp = new Notes();
    	
    	ArrayList<Adresse> lstAdd = new ArrayList<Adresse>(); 
    	ArrayList<Notes> lstNotes = new ArrayList<Notes>();
   
    	
    	
    	//	Boucle de remplissage de la base de données
    	for (int i = 1; i < 22; i++) {
    		
    		
    		
        	emp_temp.setLogin("fl" + i);	
        	emp_temp.setPassword("pass");
        	emp_temp.setNom("FERT" + i);
        	emp_temp.setPrenom("Lucy" + i);
        	emp_temp.setEmail("FL" + i + "@ajc.fr");
        	emp_temp.setRole("DRH" + i);
        	
        	add_temp.setNumRue(i);
        	add_temp.setRue("rue de la Playa");
        	add_temp.setCP(24528);
        	add_temp.setVille("Vacation");
        	
        
        	add_temp.setEmployee(emp_temp);
        	
        	
        	
        	
        	lstAdd.add(add_temp);
        	emp_temp.setAdresse(lstAdd);
        	
        	
        	notes_temp.setSport(i/2);
        	notes_temp.setSocial(i/2);
        	notes_temp.setPerformance(4);
        	notes_temp.setAssiduite(5);
        	notes_temp.setAnnee(2019);
        	
        	
        	notes_temp.setEmployee(emp_temp);
        	
        	 
        	lstNotes.add(notes_temp);
        	emp_temp.setNotes(lstNotes);
        	
        	
        	empService.createOrUpdateEmployee(emp_temp);

        	
        	emp_temp = new Employee();
        	add_temp = new Adresse();
        	notes_temp = new Notes();
        	
        	lstAdd = new ArrayList<Adresse>(); 
        	lstNotes = new ArrayList<Notes>();
        
        	
        	
        	
		}
    	
    	
 
    	
    	
    	
    	
    	
    	//  Modification de l'employé n° 5
    	
    	emp_temp = empService.findByLoginAndPassword("fl5", "pass");
    	
    	emp_temp.setRole("DRH++");
    	empService.createOrUpdateEmployee(emp_temp);
    	
    	
    	//	Suppression de l'employé n° 20
    	empService.deleteById(20);
    	
    	
    	
    	// 	Lecture
    	List<Employee> empList = empService.findAll();

    	System.out.println("Liste des Employees : ");
    	for (Employee employee : empList) {
    		System.out.println(employee);
		}
    	
    	
    	
    	
    	
    	//Affichege par page
    	

    			// Creation
    			PagedListHolder page = new PagedListHolder(empList);
    			page.setPageSize(10); 

    			
    			
    			System.out.println("");
    			System.out.println("Affichage page par page : ");
    			System.out.println("Page 1 :");
    			int numPage = 0;  // Ne marche pas encore pour la deuxième page
    			page.setPage(numPage);  //Choisir la page ici
    	
    			for (int i = 0; i < page.getPageSize(); i++) {
    				System.out.println(page.getPageList().get(i));
				}
    			

    			
    			
    	
    	
    	// Meilleur employé 2019
    	
    	NotesService NotesService = context.getBean(NotesService.class);
    	System.out.println("");
    	System.out.println("La moyenne d'un employé au hasard : " + NotesService.finById(5).getMoyenne());
    	
    	
    	
    	
    	
    	List<Notes> notesList = NotesService.findAll();
    	
    	double temp_moy1 = 0, temp_moy2 = 0, temp_moy3 = 0;
    	
    	Employee emp1 = new Employee();
    	
    	for (Notes notes : notesList) {
    		if (notes.getMoyenne() > temp_moy1) {
    			emp1 = notes.getEmployee();
			}
    		
    
		}
    	
    	System.out.println("");
    	System.out.println("Le meuilleurs employés de 2019 est : \n " + emp1);

    	
    	
    	
    	
    	
    	
    	
    	
    	
    	


    	
    	
    	


    	
    	
    	
    	
    	
    	
    }
    
    
    
    
    
    
    
    
    
    
}
