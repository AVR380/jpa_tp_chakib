package spring.config;



import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;



@Configuration
@ComponentScan("spring")			//basePackages= {"spring.config","spring.services"})
public class AppConfig {
	
}
