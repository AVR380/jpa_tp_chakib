package spring.models;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;






@Entity
@Table(name="adresse")
public class Adresse {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_adresse;
	private int numRue;
	private String rue;
	private int CP;
	private String ville;
	
	
	@ManyToOne(
			cascade = {CascadeType.ALL}
	)
	@JoinColumn(name="id_employee", referencedColumnName="id")
	private Employee employee;
	
 

	
	
	public Adresse() {
	}




	public Adresse(int id_adresse, int numRue, String rue, int cP, String ville, Employee employee) {
		super();
		this.id_adresse = id_adresse;
		this.numRue = numRue;
		this.rue = rue;
		CP = cP;
		this.ville = ville;
		this.employee = employee;
	}




	public int getId_adresse() {
		return id_adresse;
	}
	public void setId_adresse(int id_adresse) {
		this.id_adresse = id_adresse;
	}
	public int getNumRue() {
		return numRue;
	}
	public void setNumRue(int numRue) {
		this.numRue = numRue;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public int getCP() {
		return CP;
	}
	public void setCP(int cP) {
		CP = cP;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}




	@Override
	public String toString() {
		return "Adresse [id_adresse=" + id_adresse + ", numRue=" + numRue + ", rue=" + rue + ", CP=" + CP + ", ville="
				+ ville + ", employee=" + employee + "]";
	}


	
	
	
	
	
	
	
	
	

}
