package spring.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;








@Entity
@Table(name="employee")
public class Employee {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String login;

	private String password;

	private String nom;
	
	private String prenom;
	
	private String email;
	
	private String role;
	
	
	@OneToMany(mappedBy="employee",
			cascade = {CascadeType.ALL})
	private List<Adresse> adresse;
  
	
	@OneToMany(mappedBy="employee",
			cascade = {CascadeType.ALL})
	private List<Notes> notes;
	
	

	
	
	
	public Employee() {
	}



	
	public Employee(int id, String login, String password, String nom, String prenom, String email, String role,
			List<Adresse> adresse, List<Notes> notes) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.role = role;
		this.adresse = adresse;
		this.notes = notes;
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<Adresse> getAdresse() {
		return adresse;
	}

	public void setAdresse(List<Adresse> adresse) {
		this.adresse = adresse;
	}

	
	
	public List<Notes> getNotes() {
		return notes;
	}




	public void setNotes(List<Notes> notes) {
		this.notes = notes;
	}




	@Override
	public String toString() {
		return "Employee [id=" + id + ", login=" + login + ", password=" + password + ", nom=" + nom + ", prenom="
				+ prenom + ", email=" + email + ", role=" + role + "]";
	}
	
	
	
	
	
	
	

}
