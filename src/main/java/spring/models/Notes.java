package spring.models;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;






@Entity
@Table(name="notes")
public class Notes {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_note;
	private int sport;
	private int social;
	private int performance;
	private int assiduite;
	private int annee;

	
	
	@ManyToOne(
			cascade = {CascadeType.ALL}
	)
	@JoinColumn(name="id_employee", referencedColumnName="id")
	private Employee employee;
	
 
	
	
	
	public double getMoyenne() {
		return (sport+social+performance+assiduite)/4;
	}
	
	

	
	
	public Notes() {
	}


	public Notes(int id_note, int sport, int social, int performance, int assiduite, int annee, Employee employee) {
		super();
		this.id_note = id_note;
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.annee = annee;
		this.employee = employee;
	}



	public int getId_note() {
		return id_note;
	}


	public void setId_note(int id_note) {
		this.id_note = id_note;
	}


	public int getSport() {
		return sport;
	}


	public void setSport(int sport) {
		this.sport = sport;
	}


	public int getSocial() {
		return social;
	}


	public void setSocial(int social) {
		this.social = social;
	}


	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int performance) {
		this.performance = performance;
	}

	public int getAssiduite() {
		return assiduite;
	}

	public void setAssiduite(int assiduite) {
		this.assiduite = assiduite;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public int getAnnee() {
		return annee;
	}


	public void setAnnee(int annee) {
		this.annee = annee;
	}


	@Override
	public String toString() {
		return "Notes [id_note=" + id_note + ", sport=" + sport + ", social=" + social + ", performance=" + performance
				+ ", assiduite=" + assiduite + ", annee=" + annee + ", employee=" + employee + "]";
	}

	
	


	

}
