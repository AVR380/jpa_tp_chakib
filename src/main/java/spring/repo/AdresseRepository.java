package spring.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import spring.models.Adresse;




@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Integer>{


	public Adresse findById(int id);
	public Page<Adresse> findAll(Pageable pageable);
	public void deleteById(int id);
	
	
	
	
}
