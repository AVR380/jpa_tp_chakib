package spring.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import spring.models.Notes;




@Repository
public interface NotesRepository extends JpaRepository<Notes, Integer>{


	public Notes findById(int id);
	public Page<Notes> findAll(Pageable pageable);
	public void deleteById(int id);
	
	
	
	
}
