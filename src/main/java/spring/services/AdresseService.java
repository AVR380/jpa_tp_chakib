package spring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import spring.models.Adresse;
import spring.models.Employee;
import spring.repo.AdresseRepository;





@Service
public class AdresseService {
	
	@Autowired
	private AdresseRepository adresseRepository;
	

	
	public void createOrUpdateAdresse(Adresse add) {
		this.adresseRepository.save(add);
		
	}
	
	public void deleteadresse(Adresse add) {
		this.adresseRepository.delete(add);
	}

	
	public Adresse finById(int id) {
		return this.adresseRepository.findById(id);
	}
	
	
	
	public List<Adresse> findAll() {
		return this.adresseRepository.findAll();
	}


	
	public Page<Adresse> findAll(Pageable pageable) {
		return this.adresseRepository.findAll(pageable);
	}
	
	

 
	
	
	
	
	
	
	
	
	
	

}
