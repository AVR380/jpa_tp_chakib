package spring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import spring.models.Adresse;
import spring.models.Employee;
import spring.models.Notes;
import spring.repo.AdresseRepository;
import spring.repo.NotesRepository;





@Service
public class NotesService {
	
	@Autowired
	private NotesRepository notesRepository;
	

	public void createOrUpdateNotes(Notes notes) {
		this.notesRepository.save(notes);
		
	}
	
	public void deleteNotes(Notes notes) {
		this.notesRepository.delete(notes);
	}

	
	public Notes finById(int id) {
		return this.notesRepository.findById(id);
	}
	
	
	
	public List<Notes> findAll() {
		return this.notesRepository.findAll();
	}


	
	public Page<Notes> findAll(Pageable pageable) {
		return this.notesRepository.findAll(pageable);
	}
	
	

 
	
	
	
	
	
	
	
	
	
	

}
